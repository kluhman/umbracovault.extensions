﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Attributes;
using UmbracoVault.Extensions.Archetype.Handlers;

namespace UmbracoVault.Extensions.Archetype.Attributes
{
    /// <summary>
    ///     Marks a Vault model property as a single Archetype model. If multiple Archetypes are present, the first will be used.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ArchetypePropertyAttribute : UmbracoPropertyAttribute
    {
        public ArchetypePropertyAttribute()
        {
            TypeHandler = new ArchetypeTypeHandler();
        }

        public ArchetypePropertyAttribute(string alias) : base(alias)
        {
            TypeHandler = new ArchetypeTypeHandler();
        }
    }
}