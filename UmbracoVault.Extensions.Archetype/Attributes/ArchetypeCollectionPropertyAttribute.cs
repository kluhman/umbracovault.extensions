﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Attributes;
using UmbracoVault.Extensions.Archetype.Handlers;

namespace UmbracoVault.Extensions.Archetype.Attributes
{
    /// <summary>
    ///     Marks a Vault model property as a collection of Archetype models
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ArchetypeCollectionPropertyAttribute : UmbracoPropertyAttribute
    {
        public ArchetypeCollectionPropertyAttribute()
        {
            TypeHandler = new ArchetypeCollectionTypeHandler();
        }

        public ArchetypeCollectionPropertyAttribute(string alias) : base(alias)
        {
            TypeHandler = new ArchetypeCollectionTypeHandler();
        }
    }
}