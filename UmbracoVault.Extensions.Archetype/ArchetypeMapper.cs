﻿using System;
using System.Collections.Generic;
using System.Linq;

using Archetype.Models;

namespace UmbracoVault.Extensions.Archetype
{
    internal static class ArchetypeMapper
    {
        public static ICollection<T> MapArchetypes<T>(ArchetypeModel archetype)
        {
            if (archetype == null || !archetype.Any())
            {
                return new List<T>();
            }

            var models = new List<T>();
            foreach (var item in archetype)
            {
                var model = Activator.CreateInstance<T>();
                Vault.Context.FillClassProperties(model, (alias, propertyInfo, recursive) =>
                {
                    return item.Properties.FirstOrDefault(p => p.Alias == alias)?.Value;
                });

                models.Add(model);
            }

            return models;
        }
    }
}