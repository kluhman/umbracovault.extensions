﻿using System;
using System.Collections.Generic;
using System.Linq;

using Archetype.Models;

using Newtonsoft.Json;

namespace UmbracoVault.Extensions.Archetype.Handlers
{
    public abstract class BaseArchetypeTypeHandler
    {
        protected static ICollection<T> ParseModels<T>(object input)
        {
            try
            {
                if (input == null)
                {
                    return new List<T>();
                }

                var archetype = input as ArchetypeModel ?? JsonConvert.DeserializeObject<ArchetypeModel>(input.ToString());
                return ArchetypeMapper.MapArchetypes<T>(archetype);
            }
            catch
            {
                return new List<T>();
            }
        }
    }
}