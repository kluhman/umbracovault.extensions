﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Attributes;
using UmbracoVault.TypeHandlers;

namespace UmbracoVault.Extensions.Archetype.Handlers
{
    [IgnoreTypeHandlerAutoRegistration]
    internal class ArchetypeTypeHandler : BaseArchetypeTypeHandler, ITypeHandler
    {
        public object GetAsType<T>(object input)
        {
            return ParseModels<T>(input).FirstOrDefault();
        }

        public Type TypeSupported => GetType();
    }
}