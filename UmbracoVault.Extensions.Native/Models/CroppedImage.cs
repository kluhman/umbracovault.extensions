﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

namespace UmbracoVault.Extensions.Native.Models
{
    public class CroppedImage
    {
        [JsonProperty("src")]
        public string Url { get; set; }

        [JsonProperty("focalPoint")]
        public ImageFocalPoint FocalPoint { get; set; }

        [JsonProperty("crops")]
        public List<Crop> Crops { get; set; }

        public class Crop
        {
            [JsonProperty("alias")]
            public string Alias { get; set; }

            [JsonProperty("width")]
            public int Width { get; set; }

            [JsonProperty("height")]
            public int Height { get; set; }
        }
    }
}