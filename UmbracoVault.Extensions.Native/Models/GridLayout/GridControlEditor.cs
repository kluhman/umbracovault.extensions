﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

using UmbracoVault.Extensions.Native.GridConverters.Configs;

namespace UmbracoVault.Extensions.Native.Models.GridLayout
{
    public class GridControlEditor
    {
        [JsonProperty(PropertyName = "alias")]
        public string Alias { get; set; }

        [JsonProperty(PropertyName = "view")]
        public string View { get; set; }

        [JsonProperty(PropertyName = "render")]
        public string RenderOverride { get; set; }

        [JsonProperty(PropertyName = "config")]
        public dynamic Config { get; set; }

        public object GetConfig()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Alias))
                {
                    return Config;
                }

                var converter = GridConfigConverterResolver.Instance.Resolve(Alias);
                if (converter == null)
                {
                    return Config;
                }

                return converter.Convert(Config);
            }
            catch
            {
                return Config;
            }
        }

        public T GetConfig<T>()
        {
            return (T)GetConfig();
        }
    }
}