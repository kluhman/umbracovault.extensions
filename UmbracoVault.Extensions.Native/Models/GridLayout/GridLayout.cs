﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

namespace UmbracoVault.Extensions.Native.Models.GridLayout
{
    public class GridLayout
    {
        [JsonProperty(PropertyName = "sections")]
        public ICollection<GridSection> Sections { get; set; } = new List<GridSection>();
    }
}