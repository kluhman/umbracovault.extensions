﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

namespace UmbracoVault.Extensions.Native.Models.GridLayout
{
    public abstract class BaseGridElement
    {
        [JsonProperty(PropertyName = "styles")]
        public IDictionary<string, string> Styles { get; set; } = new Dictionary<string, string>();

        [JsonProperty(PropertyName = "config")]
        public IDictionary<string, string> Attributes { get; set; } = new Dictionary<string, string>();
    }
}