﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

namespace UmbracoVault.Extensions.Native.Models.GridLayout
{
    public class GridCell : BaseGridElement
    {
        [JsonProperty(PropertyName = "grid")]
        public int CellSize { get; set; }

        [JsonProperty(PropertyName = "controls")]
        public ICollection<GridControl> Controls { get; set; } = new List<GridControl>();
    }
}