﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

namespace UmbracoVault.Extensions.Native.Models.GridLayout
{
    public class GridSection
    {
        [JsonProperty(PropertyName = "grid")]
        public int ColumnSize { get; set; }

        [JsonProperty(PropertyName = "rows")]
        public ICollection<GridRow> Rows { get; set; } = new List<GridRow>();
    }
}