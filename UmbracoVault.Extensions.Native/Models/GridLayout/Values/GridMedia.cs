﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

namespace UmbracoVault.Extensions.Native.Models.GridLayout.Values
{
    public class GridMedia
    {
        [JsonProperty(PropertyName = "id")]
        public int ImageId { get; set; }

        [JsonProperty(PropertyName = "image")]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "focalPoint")]
        public ImageFocalPoint FocalPoint { get; set; }

        [JsonProperty(PropertyName = "altText")]
        public string AltText { get; set; }

        [JsonProperty(PropertyName = "caption")]
        public string Caption { get; set; }
    }
}