﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using Newtonsoft.Json;

namespace UmbracoVault.Extensions.Native.Models.GridLayout.Values
{
    public class GridMacro
    {
        [JsonProperty(PropertyName = "macroAlias")]
        public string MacroAlias { get; set; }

        [JsonProperty(PropertyName = "macroParamsDictionary")]
        public ViewDataDictionary Parameters { get; set; } = new ViewDataDictionary();
    }
}