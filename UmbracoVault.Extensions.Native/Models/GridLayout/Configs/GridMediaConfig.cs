﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

namespace UmbracoVault.Extensions.Native.Models.GridLayout.Configs
{
    public class GridMediaConfig
    {
        [JsonProperty(PropertyName = "size")]
        public ImageSize Size { get; set; }
    }
}