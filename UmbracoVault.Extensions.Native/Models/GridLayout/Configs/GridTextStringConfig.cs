﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

namespace UmbracoVault.Extensions.Native.Models.GridLayout.Configs
{
    public class GridTextStringConfig
    {
        [JsonProperty(PropertyName = "style")]
        public string Style { get; set; }

        [JsonProperty(PropertyName = "markup")]
        public string Markup { get; set; }
    }
}