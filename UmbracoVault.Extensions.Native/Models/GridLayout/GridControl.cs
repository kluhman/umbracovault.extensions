using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

using UmbracoVault.Extensions.Native.GridConverters.Values;

namespace UmbracoVault.Extensions.Native.Models.GridLayout
{
    public class GridControl
    {
        [JsonProperty(PropertyName = "editor")]
        public GridControlEditor Editor { get; set; }

        [JsonProperty(PropertyName = "value")]
        public dynamic Value { get; set; }

        public object GetValue()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Editor?.Alias))
                {
                    return Value;
                }

                var converter = GridValueConverterResolver.Instance.Resolve(Editor.Alias);
                if (converter == null)
                {
                    return Value;
                }

                return converter.Convert(Value);
            }
            catch
            {
                return Value;
            }
        }

        public T GetValue<T>()
        {
            return (T)GetValue();
        }
    }
}