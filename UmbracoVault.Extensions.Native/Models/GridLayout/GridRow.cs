﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

namespace UmbracoVault.Extensions.Native.Models.GridLayout
{
    public class GridRow : BaseGridElement
    {
        [JsonProperty(PropertyName = "areas")]
        public ICollection<GridCell> Cells { get; set; } = new List<GridCell>();
    }
}