﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

namespace UmbracoVault.Extensions.Native.GridConverters
{
    public class JsonGridConverter<TModel> : IGridConverter
    {
        public object Convert(object value)
        {
            try
            {
                var json = value?.ToString();
                if (json == null)
                {
                    return null;
                }

                return JsonConvert.DeserializeObject<TModel>(json);
            }
            catch
            {
                return null;
            }
        }
    }
}