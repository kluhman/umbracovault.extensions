﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UmbracoVault.Extensions.Native.GridConverters
{
    public abstract class BaseConverterResolver
    {
        private readonly IDictionary<string, IGridConverter> _converters;

        protected BaseConverterResolver()
        {
            _converters = InitializeConverters();
        }

        public void RegisterValueConverter(string editorAlias, IGridConverter converter)
        {
            if (_converters.ContainsKey(editorAlias))
            {
                _converters[editorAlias] = converter;
            }
            else
            {
                _converters.Add(editorAlias, converter);
            }
        }

        internal IGridConverter Resolve(string editorAlias)
        {
            return _converters.ContainsKey(editorAlias) ? _converters[editorAlias] : null;
        }

        protected abstract IDictionary<string, IGridConverter> InitializeConverters();
    }
}