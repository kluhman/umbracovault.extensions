﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Extensions.Native.Models.GridLayout.Configs;

namespace UmbracoVault.Extensions.Native.GridConverters.Configs
{
    public class GridConfigConverterResolver : BaseConverterResolver
    {
        private static readonly object _lock = new object();
        private static GridConfigConverterResolver _instance;

        private GridConfigConverterResolver()
        {
            
        }

        public static GridConfigConverterResolver Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_lock)
                    {
                        if (_instance == null)
                        {
                            _instance = new GridConfigConverterResolver();
                        }
                    }
                }

                return _instance;
            }
        }

        protected override IDictionary<string, IGridConverter> InitializeConverters()
        {
            return new Dictionary<string, IGridConverter>
            {
                { "media_wide_cropped", new JsonGridConverter<GridMediaConfig>() },
                { "banner_headline", new JsonGridConverter<GridTextStringConfig>() },
                { "banner_tagline", new JsonGridConverter<GridTextStringConfig>() },
                { "headline", new JsonGridConverter<GridTextStringConfig>() },
                { "headline_centered", new JsonGridConverter<GridTextStringConfig>() },
                { "abstract", new JsonGridConverter<GridTextStringConfig>() },
                { "paragraph", new JsonGridConverter<GridTextStringConfig>() },
                { "quote", new JsonGridConverter<GridTextStringConfig>() },
                { "code", new JsonGridConverter<GridTextStringConfig>() },
            };
        }
    }
}