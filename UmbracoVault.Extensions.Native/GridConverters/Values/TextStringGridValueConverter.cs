﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UmbracoVault.Extensions.Native.GridConverters.Values
{
    public class TextStringGridValueConverter : IGridConverter
    {
        public object Convert(object value)
        {
            return value?.ToString();
        }
    }
}