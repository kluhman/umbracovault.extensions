﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.TypeHandlers;

namespace UmbracoVault.Extensions.Native.GridConverters.Values
{
    public class RichTextGridValueConverter : IGridConverter
    {
        public object Convert(object value)
        {
            return new RichTextTypeHandler().GetAsType<string>(value?.ToString());
        }
    }
}