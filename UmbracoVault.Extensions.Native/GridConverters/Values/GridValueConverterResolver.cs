﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Extensions.Native.Models.GridLayout.Values;

namespace UmbracoVault.Extensions.Native.GridConverters.Values
{
    public class GridValueConverterResolver : BaseConverterResolver
    {
        private static readonly object _lock = new object();
        private static GridValueConverterResolver _instance;

        private GridValueConverterResolver()
        {

        }

        public static GridValueConverterResolver Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_lock)
                    {
                        if (_instance == null)
                        {
                            _instance = new GridValueConverterResolver();
                        }
                    }
                }

                return _instance;
            }
        }

        protected override IDictionary<string, IGridConverter> InitializeConverters()
        {
            return new Dictionary<string, IGridConverter>
            {
                { "rte", new RichTextGridValueConverter() },
                { "embed", new RichTextGridValueConverter() },
                { "media", new JsonGridConverter<GridMedia>() },
                { "media_wide", new JsonGridConverter<GridMedia>() },
                { "media_wide_cropped", new JsonGridConverter<GridMedia>() },
                { "banner_headline", new TextStringGridValueConverter() },
                { "banner_tagline", new TextStringGridValueConverter() },
                { "headline", new TextStringGridValueConverter() },
                { "headline_centered", new TextStringGridValueConverter() },
                { "abstract", new TextStringGridValueConverter() },
                { "paragraph", new TextStringGridValueConverter() },
                { "quote", new TextStringGridValueConverter() },
                { "code", new TextStringGridValueConverter() },
                { "macro", new JsonGridConverter<GridMacro>() }
            };
        }
    }
}
