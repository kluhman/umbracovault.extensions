﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UmbracoVault.Extensions.Native.GridConverters
{
    public interface IGridConverter
    {
        object Convert(object value);
    }
}