﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Attributes;
using UmbracoVault.Extensions.Native.Handlers;

namespace UmbracoVault.Extensions.Native.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class UmbracoImageCropperPropertyAttribute : UmbracoPropertyAttribute
    {
        public UmbracoImageCropperPropertyAttribute()
        {
            TypeHandler = new UmbracoImageCropperTypeHandler();
        }

        public UmbracoImageCropperPropertyAttribute(string alias) : base(alias)
        {
            TypeHandler = new UmbracoImageCropperTypeHandler();
        }
    }
}