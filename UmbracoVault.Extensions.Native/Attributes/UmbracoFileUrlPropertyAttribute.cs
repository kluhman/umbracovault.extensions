﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Attributes;
using UmbracoVault.Extensions.Native.Handlers;

namespace UmbracoVault.Extensions.Native.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class UmbracoFileUrlPropertyAttribute : UmbracoPropertyAttribute
    {
        public UmbracoFileUrlPropertyAttribute()
        {
            TypeHandler = new UmbracoFileUrlTypeHandler();
        }

        public UmbracoFileUrlPropertyAttribute(string alias) : base(alias)
        {
            TypeHandler = new UmbracoFileUrlTypeHandler();
        }
    }
}