﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Attributes;
using UmbracoVault.Extensions.Native.Handlers;

namespace UmbracoVault.Extensions.Native.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NullableDateTimeProperty : UmbracoPropertyAttribute
    {
        public NullableDateTimeProperty()
        {
            TypeHandler = new NullableDateTimeTypeHandler();
        }

        public NullableDateTimeProperty(string alias) : base(alias)
        {
            TypeHandler = new NullableDateTimeTypeHandler();
        }
    }
}