﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Attributes;
using UmbracoVault.Extensions.Native.Handlers;

namespace UmbracoVault.Extensions.Native.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class UmbracoGridLayoutPropertyAttribute : UmbracoPropertyAttribute
    {
        public UmbracoGridLayoutPropertyAttribute()
        {
            TypeHandler= new UmbracoGridLayoutTypeHandler();
        }

        public UmbracoGridLayoutPropertyAttribute(string alias) : base(alias)
        {
            TypeHandler= new UmbracoGridLayoutTypeHandler();
        }
    }
}