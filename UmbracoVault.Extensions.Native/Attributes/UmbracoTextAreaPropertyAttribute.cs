﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Attributes;
using UmbracoVault.Extensions.Native.Handlers;

namespace UmbracoVault.Extensions.Native.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class UmbracoTextAreaPropertyAttribute : UmbracoPropertyAttribute
    {
        public UmbracoTextAreaPropertyAttribute()
        {
            TypeHandler = new UmbracoTextAreaTypeHandler();
        }

        public UmbracoTextAreaPropertyAttribute(string alias) : base(alias)
        {
            TypeHandler = new UmbracoTextAreaTypeHandler();
        }
    }
}