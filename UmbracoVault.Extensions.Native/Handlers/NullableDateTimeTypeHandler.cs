﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Attributes;
using UmbracoVault.TypeHandlers;
using UmbracoVault.TypeHandlers.Primitives;

namespace UmbracoVault.Extensions.Native.Handlers
{
    [IgnoreTypeHandlerAutoRegistration]
    internal class NullableDateTimeTypeHandler : ITypeHandler
    {
        public object GetAsType<T>(object input)
        {
            try
            {
                if (input == null)
                {
                    return null;
                }

                if (input is DateTime)
                {
                    return (DateTime)input == DateTime.MinValue || DateTime.Parse(DateTime.MinValue.ToString()) == (DateTime)input ? null : input;
                }

                return new DateTimeTypeHandler().GetAsType<DateTime>(input);
            }
            catch
            {
                return null;
            }
        }

        public Type TypeSupported => typeof(DateTime?);
    }
}