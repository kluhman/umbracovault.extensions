﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Attributes;
using UmbracoVault.TypeHandlers;

namespace UmbracoVault.Extensions.Native.Handlers
{
    [IgnoreTypeHandlerAutoRegistration]
    internal class UmbracoTextAreaTypeHandler : ITypeHandler
    {
        public object GetAsType<T>(object input)
        {
            return input?.ToString().Replace("\n", "<br/>");
        }

        public Type TypeSupported => typeof(string);
    }
}