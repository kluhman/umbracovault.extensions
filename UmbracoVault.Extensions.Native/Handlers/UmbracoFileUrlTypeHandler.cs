﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Attributes;
using UmbracoVault.TypeHandlers;

namespace UmbracoVault.Extensions.Native.Handlers
{
    [IgnoreTypeHandlerAutoRegistration]
    internal class UmbracoFileUrlTypeHandler : ITypeHandler
    {
        public object GetAsType<T>(object input)
        {
            try
            {
                var urlOrJson = input as string ?? input?.ToString();
                if (Uri.IsWellFormedUriString(urlOrJson, UriKind.RelativeOrAbsolute))
                {
                    return urlOrJson;
                }

                return UmbracoImageCropperTypeHandler.GetCroppedImage(urlOrJson)?.Url;
            }
            catch
            {
                return null;
            }
        }

        public Type TypeSupported => typeof(string);
    }
}