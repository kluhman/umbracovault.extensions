﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

using UmbracoVault.Extensions.Native.Models;
using UmbracoVault.TypeHandlers;

namespace UmbracoVault.Extensions.Native.Handlers
{
    public class UmbracoImageCropperTypeHandler : ITypeHandler
    {
        public object GetAsType<T>(object input)
        {
            return GetCroppedImage(input?.ToString());
        }

        internal static CroppedImage GetCroppedImage(string input)
        {
            try
            {
                return JsonConvert.DeserializeObject<CroppedImage>(input);
            }
            catch
            {
                return null;
            }
        }

        public Type TypeSupported => typeof(CroppedImage);
    }
}