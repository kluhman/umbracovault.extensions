﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

using UmbracoVault.Extensions.Native.Models.GridLayout;
using UmbracoVault.TypeHandlers;

namespace UmbracoVault.Extensions.Native.Handlers
{
    public class UmbracoGridLayoutTypeHandler : ITypeHandler
    {
        public object GetAsType<T>(object input)
        {
            try
            {
                var json = input?.ToString();
                if (json == null)
                {
                    return null;
                }

                return JsonConvert.DeserializeObject<GridLayout>(json);
            }
            catch
            {
                return null;
            }
        }

        public Type TypeSupported => typeof(GridLayout);
    }
}