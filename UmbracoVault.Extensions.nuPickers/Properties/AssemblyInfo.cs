﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

using UmbracoVault.Attributes;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("UmbracoVault.Extensions.nuPickers")]
[assembly: AssemblyDescription("An UmbracoVault extension library for the nuPickers data types. Allows for deserializing the Picker data and auto converting picked values to enums.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Kaleb Luhman")]
[assembly: AssemblyProduct("UmbracoVault.Extensions.nuPickers")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("02812aa8-cd59-4ea7-9296-8301da63eedf")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]

[assembly:ContainsUmbracoVaultTypeHandlers]