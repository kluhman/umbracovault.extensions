﻿using System;
using System.Collections.Generic;
using System.Linq;

using nuPickers;

using UmbracoVault.TypeHandlers;

namespace UmbracoVault.Extensions.nuPickers.Handlers
{
    internal class NuPickerEnumCollectionTypeHandler : ITypeHandler
    {
        public object GetAsType<T>(object input)
        {
            var picker = new NuPickerTypeHandler().GetAsType<T>(input) as Picker;
            if (picker == null)
            {
                return new List<T>();
            }

            return picker.AsEnums().Cast<T>().ToList();
        }

        public Type TypeSupported => GetType();
    }
}