﻿using System;
using System.Collections.Generic;
using System.Linq;

using nuPickers;

using Newtonsoft.Json;

using UmbracoVault.TypeHandlers;

namespace UmbracoVault.Extensions.nuPickers.Handlers
{
    /// <summary>
    ///     Maps Umbraco properties to <see cref="Picker"/>
    /// </summary>
    public class NuPickerTypeHandler : ITypeHandler
    {
        /// <summary>
        ///     Gets input data as a <see cref="Picker"/>
        /// </summary>
        /// <typeparam name="T">Not used</typeparam>
        /// <param name="input">Input data</param>
        /// <returns><see cref="Picker"/></returns>
        public object GetAsType<T>(object input)
        {
            try
            {
                if (input == null)
                {
                    return null;
                }

                return input as Picker ?? JsonConvert.DeserializeObject<Picker>(input.ToString());
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Type suppported by handler
        /// </summary>
        public Type TypeSupported => typeof(Picker);
    }
}