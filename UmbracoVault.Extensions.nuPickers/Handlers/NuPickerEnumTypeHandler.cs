﻿using System;
using System.Collections.Generic;
using System.Linq;

using nuPickers;

using UmbracoVault.TypeHandlers;

namespace UmbracoVault.Extensions.nuPickers.Handlers
{
    internal class NuPickerEnumTypeHandler : ITypeHandler
    {
        internal bool IsNullable { get; set; }

        public object GetAsType<T>(object input)
        {
            var picker = new NuPickerTypeHandler().GetAsType<Picker>(input) as Picker;
            if (picker == null || !picker.PickedKeys.Any())
            {
                if (IsNullable)
                {
                    return null;
                }

                return default(T);
            }

            return picker.AsEnums().Cast<T>().FirstOrDefault();
        }

        public Type TypeSupported => GetType();
    }
}