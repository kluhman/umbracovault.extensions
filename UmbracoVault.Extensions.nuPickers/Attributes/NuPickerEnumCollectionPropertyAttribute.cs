﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Attributes;
using UmbracoVault.Extensions.nuPickers.Handlers;

namespace UmbracoVault.Extensions.nuPickers.Attributes
{
    /// <summary>
    ///     Marks a Vault model property as a nuPicker collection of enums
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NuPickerEnumCollectionPropertyAttribute : UmbracoPropertyAttribute
    {
        public NuPickerEnumCollectionPropertyAttribute()
        {
            TypeHandler = new NuPickerEnumCollectionTypeHandler();
        }

        public NuPickerEnumCollectionPropertyAttribute(string alias) : base(alias)
        {
            TypeHandler = new NuPickerEnumCollectionTypeHandler();
        }
    }
}