﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Attributes;
using UmbracoVault.Extensions.nuPickers.Handlers;

namespace UmbracoVault.Extensions.nuPickers.Attributes
{
    /// <summary>
    ///     Marks a Vault model property as a nuPicker enum
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NuPickerEnumPropertyAttribute : UmbracoPropertyAttribute
    {
        private bool _isNullable;

        /// <summary>
        ///     Allows the enum to be nullable. If false and an enum is not selected, the default value for the enum type will be used
        /// </summary>
        public bool IsNullable
        {
            get { return _isNullable; }
            set
            {
                _isNullable = value;

                var handler = TypeHandler as NuPickerEnumTypeHandler;
                if (handler != null)
                {
                    handler.IsNullable = _isNullable;
                }
            }
        }

        public NuPickerEnumPropertyAttribute()
        {
            TypeHandler = new NuPickerEnumTypeHandler();
        }

        public NuPickerEnumPropertyAttribute(string alias) : base(alias)
        {
            TypeHandler = new NuPickerEnumTypeHandler();
        }
    }
}