﻿using System;
using System.Collections.Generic;
using System.Linq;

using nuPickers;

using UmbracoVault.Attributes;
using UmbracoVault.Extensions.nuPickers.Handlers;

namespace UmbracoVault.Extensions.nuPickers.Attributes
{
    /// <summary>
    ///     Marks a Vault model property as a <see cref="Picker" />
    /// </summary>
    public class NuPickerProperty : UmbracoPropertyAttribute
    {
        public NuPickerProperty()
        {
            TypeHandler = new NuPickerTypeHandler();
        }

        public NuPickerProperty(string alias) : base(alias)
        {
            TypeHandler = new NuPickerTypeHandler();
        }
    }
}