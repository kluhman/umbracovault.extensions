﻿using System;
using System.Collections.Generic;
using System.Linq;

using RJP.MultiUrlPicker.Models;

using UmbracoVault.Attributes;
using UmbracoVault.Extensions.Archetype.Attributes;
using UmbracoVault.Extensions.MultiUrlPicker.Attributes;

namespace UmbracoVault.Extensions.TestSite.ViewModels.DataTypes
{
    [UmbracoEntity(AutoMap = true)]
    public class RandomArchetype
    {
        [UmbracoRichTextProperty]
        public string Richtext { get; set; }

        public string Textstring { get; set; }

        [ArchetypeProperty]
        public CarouselItem CarouselItem { get; set; }

        public Link UrlPicker { get; set; }

        [MultiUrlPickerProperty]
        public MultiUrls MultiUrlPicker { get; set; }

        public MediaItem Media { get; set; }

        public BaseViewModel ContentPicker { get; set; }
    }
}