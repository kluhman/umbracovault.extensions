﻿using System;
using System.Collections.Generic;
using System.Linq;

using RJP.MultiUrlPicker.Models;

using UmbracoVault.Attributes;

namespace UmbracoVault.Extensions.TestSite.ViewModels.DataTypes
{
    [UmbracoEntity(AutoMap = true)]
    public class CarouselItem
    {
        public string Heading { get; set; }

        public string Description { get; set; }

        public MediaItem Image { get; set; }

        public Link CtaLink { get; set; }
    }
}