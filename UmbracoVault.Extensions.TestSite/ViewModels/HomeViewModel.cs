﻿using System;
using System.Collections.Generic;
using System.Linq;

using RJP.MultiUrlPicker.Models;

using UmbracoVault.Attributes;
using UmbracoVault.Extensions.Archetype.Attributes;
using UmbracoVault.Extensions.MultiUrlPicker.Attributes;
using UmbracoVault.Extensions.nuPickers.Attributes;
using UmbracoVault.Extensions.Native.Models.GridLayout;
using UmbracoVault.Extensions.TestSite.PickerSources;
using UmbracoVault.Extensions.TestSite.ViewModels.DataTypes;

namespace UmbracoVault.Extensions.TestSite.ViewModels
{
    [UmbracoEntity(AutoMap = true)]
    public class HomeViewModel : BaseViewModel
    {
        [ArchetypeCollectionProperty]
        public List<CarouselItem> CarouselItems { get; set; }

        //test explicit multi url mapping
        [MultiUrlPickerProperty]
        public MultiUrls HeaderLinks { get; set; }

        //test implicit multi url mapping
        public MultiUrls FooterLinks { get; set; }

        //test explicity url mapping
        [UrlPickerProperty]
        public Link FindLocationLink { get; set; }

        //test implicit url mapping
        public Link ContactUsLink { get; set; }

        [ArchetypeProperty]
        public RandomArchetype RandomArchetype { get; set; }

        [NuPickerEnumProperty]
        public SimpleEnum SimpleEnum { get; set; }

        [NuPickerEnumProperty]
        public ComplexEnum ComplexEnum { get; set; }

        [NuPickerEnumProperty(IsNullable = true)]
        public SimpleEnum? NullableEnum { get; set; }

        [NuPickerEnumCollectionProperty]    
        public ICollection<SimpleEnum> EnumCollection { get; set; }

        public GridLayout GridContent { get; set; }
    }
}