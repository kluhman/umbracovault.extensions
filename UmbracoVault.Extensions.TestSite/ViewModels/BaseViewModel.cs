﻿using System;
using System.Collections.Generic;
using System.Linq;

using Umbraco.Core.Models;

using UmbracoVault.Attributes;

namespace UmbracoVault.Extensions.TestSite.ViewModels
{
    [UmbracoEntity(AutoMap = true)]
    public class BaseViewModel
    {
        public IPublishedContent CmsContent { get; set; }
    }
}