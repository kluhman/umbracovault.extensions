﻿using System;
using System.Collections.Generic;
using System.Linq;

using Umbraco.Core;
using Umbraco.Web.Mvc;

using UmbracoVault.Controllers;

namespace UmbracoVault.Extensions.TestSite
{
    public class StartupEventHandler : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            base.ApplicationStarting(umbracoApplication, applicationContext);

            Vault.RegisterViewModelNamespace("UmbracoVault.Extensions.TestSite.ViewModels", "UmbracoVault.Extensions.TestSite");
            DefaultRenderMvcControllerResolver.Current.SetDefaultControllerType(typeof(VaultRenderMvcController));
        }
    }
}