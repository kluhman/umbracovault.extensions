﻿using System;
using System.Collections.Generic;
using System.Linq;

using nuPickers.Shared.EnumDataSource;

namespace UmbracoVault.Extensions.TestSite.PickerSources
{
    public enum SimpleEnum
    {
        SimpleOne,
        SimpleTwo,
        SimpleThree
    }

    public enum ComplexEnum
    {
        [EnumDataSource(Key = "#1", Label = "Complex One")]
        ComplexOne,

        [EnumDataSource(Key = "#2", Label = "Complex Two")]
        ComplexTwo,

        [EnumDataSource(Key = "#3", Label = "Complex Three")]
        ComplexThree
    }
}