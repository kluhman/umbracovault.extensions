# UmbracoVault.Extensions

[![Build status](https://ci.appveyor.com/api/projects/status/mkl5p0me6tagsg6i/branch/master?svg=true)](https://ci.appveyor.com/project/kluhman/umbracovault-extensions/branch/master)

This repository contains a collection of extension libraries (distributed as individual NuGet packages) for [UmbracoVault](https://github.com/thenerdery/UmbracoVault). These libraries allow Umbraco property data to be deserialized to strongly typed models.

## UmbracoVault.Extensions.Archetype

[![NuGet](https://img.shields.io/nuget/v/UmbracoVault.Extensions.Archetype.svg?style=flat-square)](https://www.nuget.org/packages/UmbracoVault.Extensions.Archetype)

This library adds support to UmbracoVault for the [Archetype](https://github.com/kgiszewski/Archetype) data type. The package provides two attributes for designating a model property as an Archetype. These attributes support a single Archetype model or a collection of Archetype models. Archetype models can contain any property type already supported by UmbracoVault, including nested Archetypes.

### Usage 

```csharp
[ArchetypeProperty]
public Person Person { get; set; }

[ArchetypeCollectionProperty]
public ICollection<Person> People { get; set; }

[UmbracoEntity(AutoMap = true)]
public class Person
{
	public string FirstName { get; set; }

	public string LastName { get; set; }

	public string Email { get; set; }
}
```

## UmbracoVault.Extensions.MultiUrlPicker

[![NuGet](https://img.shields.io/nuget/v/UmbracoVault.Extensions.MultiUrlPicker.svg?style=flat-square)](https://www.nuget.org/packages/UmbracoVault.Extensions.MultiUrlPicker)

This library adds support to UmbracoVault for the [MultiUrlPicker](https://github.com/rasmusjp/umbraco-multi-url-picker) data type. The package provides two attributes and two handlers for designating a model property as an URL Picker. These attributes support a single Link model or a MultiUrls model. Type Handlers should auto-register when Vault starts up, so Link and MultiUrls can be mapped implicitly or explicitly using the attributes.

### Usage

```csharp
public Link ImplicitSingleLink { get; set; }

public MultiUrls ImplicitMultiUrlPicker { get; set; }

[UrlPickerProperty]
public Link ExplicitSingleLink { get; set; }

[MultiUrlPickerProperty]
public MultiUrls ExplicitMultiUrlPicker { get; set; }
```

## UmbracoVault.Extensions.nuPickers

[![NuGet](https://img.shields.io/nuget/v/UmbracoVault.Extensions.nuPickers.svg?style=flat-square)](https://www.nuget.org/packages/UmbracoVault.Extensions.nuPickers)

This library adds support to UmbracoVault for the [nuPickers](https://github.com/uComponents/nuPickers) data types. The package provides three attributes and a type handler for designating nuPicker properties. The type handler will auto-register when Vault starts up, so Picker models can be mapped implicitly or explicitly using the attributes. The other attributes are for auto-mapping picker selections to Enums. 

### Usage

```csharp
public enum TestEnum
{
    [EnumDataSource(Key = "1", Label = "Option One")]
    One,
    Two,
    Three
}

[NuPickerEnumProperty]
public TestEnum SingleEnum { get; set; }

[NuPickerEnumProperty(IsNullable = true)]
public TestEnum? NullableEnum { get; set; }

[NuPickerEnumCollectionProperty]
public ICollection<TestEnum> EnumCollection { get; set; }

public Picker ImplicitPicker { get; set; }

[NuPickerProperty]
public Picker ExplicitPicker { get; set; }
```
## UmbracoVault.Extensions.Native

[![NuGet](https://img.shields.io/nuget/v/UmbracoVault.Extensions.Native.svg?style=flat-square)](https://www.nuget.org/packages/UmbracoVault.Extensions.Native)

This library adds support to UmbracoVault for data types that are native to Umbraco but do not have existing handlers in the core UmbracoVault package.

### Usage

In Umbraco 7, the image cropper is used for uploading images. This means that the stored value is no longer a pure URL as it was in Umbraco 6. The UmbracoFileUrlProperty attribute can be used to parse the cropped image object and return just the Url representation. The stored value can also be parsed into a CroppedImage model using the UmbracoImageCropperProperty attribute implicitly or explicitly.

```csharp
[UmbracoMediaEntity(AutoMap = true)]
public class MediaItem
{
	[UmbracoFileUrlProperty(Alias = "umbracoFile")]
	public string Url { get; set; }

	public CroppedImage ImplicitCroppedImage { get; set; }

	[UmbracoImageCropperProperty(Alias = "umbracoFile")]
	public CroppedImage ExplicitCroppedImage { get; set; }
}
```
