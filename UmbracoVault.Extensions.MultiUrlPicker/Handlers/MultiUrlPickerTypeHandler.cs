﻿using System;
using System.Collections.Generic;
using System.Linq;

using RJP.MultiUrlPicker.Models;

using UmbracoVault.TypeHandlers;

namespace UmbracoVault.Extensions.MultiUrlPicker.Handlers
{
    /// <summary>
    ///     Maps Umbraco properties to a <see cref="MultiUrls"/>
    /// </summary>
    public class MultiUrlPickerTypeHandler : ITypeHandler
    {
        /// <summary>
        ///     Gets input data as a <see cref="MultiUrls"/>
        /// </summary>
        /// <typeparam name="T">Not used</typeparam>
        /// <param name="input">Input data</param>
        /// <returns><see cref="MultiUrls"/></returns>
        public object GetAsType<T>(object input)
        {
            try
            {
                if (input == null)
                {
                    return null;
                }

                return input as MultiUrls ?? new MultiUrls(input.ToString());
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Type supported by handler
        /// </summary>
        public Type TypeSupported => typeof(MultiUrls);
    }
}