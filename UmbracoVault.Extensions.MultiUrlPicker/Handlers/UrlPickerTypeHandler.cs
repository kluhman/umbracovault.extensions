﻿using System;
using System.Collections.Generic;
using System.Linq;

using RJP.MultiUrlPicker.Models;

using UmbracoVault.TypeHandlers;

namespace UmbracoVault.Extensions.MultiUrlPicker.Handlers
{
    /// <summary>
    ///     Maps Umbraco properties to a single <see cref="Link"/>
    /// </summary>
    public class UrlPickerTypeHandler : ITypeHandler
    {
        /// <summary>
        ///     Gets input data as a <see cref="Link"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        public object GetAsType<T>(object input)
        {
            try
            {
                if (input == null)
                {
                    return null;
                }
                if (input is Link)
                {
                    return input;
                }
                else if (input is IEnumerable<Link>)
                {
                    return (input as IEnumerable<Link>).FirstOrDefault();
                }

                return new MultiUrls(input.ToString()).FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Type supported by handler
        /// </summary>
        public Type TypeSupported => typeof(Link);
    }
}