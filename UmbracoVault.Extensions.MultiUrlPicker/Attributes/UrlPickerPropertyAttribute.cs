﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Attributes;
using UmbracoVault.Extensions.MultiUrlPicker.Handlers;

namespace UmbracoVault.Extensions.MultiUrlPicker.Attributes
{
    /// <summary>
    ///     Marks a Vault model property as a single Url Picker. If multiple are present, the first will be used.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class UrlPickerPropertyAttribute : UmbracoPropertyAttribute
    {
        public UrlPickerPropertyAttribute()
        {
            TypeHandler = new UrlPickerTypeHandler();
        }

        public UrlPickerPropertyAttribute(string alias) : base(alias)
        {
            TypeHandler = new UrlPickerTypeHandler();
        }
    }
}