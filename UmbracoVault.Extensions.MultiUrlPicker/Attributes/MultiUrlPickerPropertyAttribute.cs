﻿using System;
using System.Collections.Generic;
using System.Linq;

using UmbracoVault.Attributes;
using UmbracoVault.Extensions.MultiUrlPicker.Handlers;

namespace UmbracoVault.Extensions.MultiUrlPicker.Attributes
{
    /// <summary>
    ///     Marks a Vault property as a list of Url Pickers
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class MultiUrlPickerPropertyAttribute : UmbracoPropertyAttribute
    {
        public MultiUrlPickerPropertyAttribute()
        {
            TypeHandler = new MultiUrlPickerTypeHandler();
        }

        public MultiUrlPickerPropertyAttribute(string alias) : base(alias)
        {
            TypeHandler = new MultiUrlPickerTypeHandler();
        }
    }
}