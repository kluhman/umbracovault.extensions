﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

using UmbracoVault.Attributes;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("UmbracoVault.Extensions.MultiUrlPicker")]
[assembly: AssemblyDescription("An UmbracoVault extension library for the MultiUrlPicker data type. Allows for Multi Url Picker properties to be deserialized to strongly typed models.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Kaleb Luhman")]
[assembly: AssemblyProduct("UmbracoVault.Extensions.MultiUrlPicker")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("bd7397c0-9d92-4ab9-8746-6eeddb8d1265")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.2")]
[assembly: AssemblyFileVersion("1.0.2")]

[assembly:ContainsUmbracoVaultTypeHandlers]
